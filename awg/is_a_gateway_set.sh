#!/bin/bash
IS_OK=1
while [ $IS_OK -eq 1 ]
   do
	GATEWAY=($(ip route show | grep default | awk '{print $3}'))
    if [[ $GATEWAY =~ [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3} ]]
        then
        IS_OK=0
    fi
    echo $GATEWAY
   done
