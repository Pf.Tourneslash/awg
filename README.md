This package is used set up Wireguard to redirect all of your traffic by your VPN, as a service.

------------------------------INSTALL-------------------------------

To install this, copy the awg directory to /etc/ (as root)
Then copy the wireguard.service to /etc/systemd/system/

Voila, now your wireguard service is set up, you can enable it with : 
sudo systemctl enable wireguard

Start it with :
sudo systemctl start wireguard
